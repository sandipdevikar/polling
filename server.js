const express=require('express');
const connectDB=require('./config/db');
const app = express();
const path= require('path');



connectDB();
app.use(express.json({ extended: false }));
app.use(express.urlencoded({extended:true}));
app.use("../public",express.static('public'))
app.use('../images',express.static('images'));
app.use('images/',express.static('images'));
app.use('images',express.static('images'));
app.use('/images',express.static('images'));

app.use(express.static('images'));
app.use(express.static(path.join(__dirname, 'public')));
app.use(express.static(path.join(__dirname, 'images')));
app.use("/api/auth",require("./routes/api/auth"));
app.use("/api/user",require("./routes/api/user"));
app.use("/api/poll",require("./routes/api/poll"));


if(process.env.NODE_ENV==='production'){
    app.use(express.static('client/build'));
    app.get('*',(req,res)=>{
        res.sendFile(path.resolve(__dirname,'client','build','index.html'));
    });
}
app.get("/",(req,res)=>{
    res.send("api running");
});

const PORT=process.env.PORT||5000;

app.listen(PORT,()=>{
    console.log("api running");
});