import React, {useState} from 'react';

import {Link,Redirect} from 'react-router-dom';
import {connect} from 'react-redux';
import { setAlert } from '../../actions/alert';
import propTypes from 'prop-types';
import { resetPassword } from '../../actions/auth';
import Alert from '../layout/Alert';

import Backdrop from '../backdrop/BackDropLoad';
import Spinner from '../layout/Spinner'
const ResetPassword = ({setAlert,resetPassword,isAuthenticated,match}) =>{

    const [formdata,setFormData]=useState({
      
        token:match.params.id,
        password:"",
        password2:""
    });


    const [hidden,setHidden]=useState(true);
const {token, password, password2}=formdata;
const onChange = e => setFormData({...formdata, [e.target.name]:e.target.value});

const onSubmit = async e => {
 e.preventDefault();
 
 if(password !==password2){
  
     setAlert("Password do not match","danger");

 }else{
  onLoad();
   resetPassword({token,password});
    
  setTimeout(onLoads,2000)
 }
}


const onLoad =()=>setHidden(false);
const onLoads = () =>setHidden(true);
  return(
    <>
    
    {!hidden&&<div onClick={()=>onLoads()}><Backdrop /><Spinner/></div>}

      
    <section className="container">
        <div className="alertDiv mt-4 p-absolute">
          <Alert/>
        </div>  
      <h1 className="large text-primary">Reset Password</h1>
      <p className="lead"><i className="fas fa-user"></i> </p>
      <form className="form" onSubmit={e=>onSubmit(e)}>
       
       
        <div className="form-group">
          <input
            type="password"
            placeholder="New Password"
            name="password"
            minLength="6" value={password} onChange={e=>onChange(e)}
          />
        </div>

        <div className="form-group">
          <input
            type="password"
            placeholder="Confirm Password"
            name="password2"
            minLength="6" value={password2} onChange={e=>onChange(e)}
          />
         
        </div>
        <div className="form-group">
          <input
            type="hidden"
          
            name="token"
            value={token} 
          />
         
        </div>
        <input type="submit" className="btn btn-primary" value="Reset" />
      </form>
     
    </section>
 </>
  );
  };
  ResetPassword .propTypes={
    setAlert:propTypes.func.isRequired,
    register:propTypes.func.isRequired,
    isAuthenticated:propTypes.bool
  };
  const mapStateToProps = state =>{
    return{
      isAuthenticated:state.auth.isAuthenticated
    }
  }
export default connect(mapStateToProps,{ setAlert, resetPassword })(ResetPassword )
