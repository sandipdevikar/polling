import React, {useState,useEffect} from 'react';
import {login} from '../../actions/auth'
import {Link,Redirect} from 'react-router-dom';
import {connect} from 'react-redux';
import Alert from '../layout/Alert'
import Backdrop from '../backdrop/BackDropLoad';
import Spinner from '../layout/Spinner'
import { url } from 'gravatar';
import './util.css';
import './main.css';
import './fonts/font-awesome-4.7.0/css/font-awesome.min.css'
const Login = ({login,isAuthenticated,loading}) =>{

  useEffect(() => {
    const script = document.createElement('script');
  
    script.src = "./vendor/jquery/jquery-3.2.1.min.js";
    script.async = true;
  
    document.body.appendChild(script);
    const script2 = document.createElement('script');
  
    script2.src = "./vendor/animsition/js/animsition.min.js";
    script2.async = true;
  
    document.body.appendChild(script2);
    const script3 = document.createElement('script');
  
    script3.src = "./vendor/bootstrap/js/popper.js";
    script3.async = true;
  
    document.body.appendChild(script3);
    const script4 = document.createElement('script');
  
    script4.src = "./vendor/jquery/jquery-3.2.1.min.js";
    script4.async = true;
  
    document.body.appendChild(script4);
    const script5 = document.createElement('script');
  
    script5.src = "./vendor/jquery/jquery-3.2.1.min.js";
    script5.async = true;
  
    document.body.appendChild(script5);
    const script6 = document.createElement('script');
  
    script6.src = "./vendor/jquery/jquery-3.2.1.min.js";
    script6.async = true;
  
    document.body.appendChild(script6);
    const script7 = document.createElement('script');
  
    script7.src = "/js/main.js";
    script7.async = true;
  
    document.body.appendChild(script7);
  
    return () => {
      document.body.removeChild(script);
      document.body.removeChild(script2);
      document.body.removeChild(script3);
      document.body.removeChild(script4);
      document.body.removeChild(script5);
      document.body.removeChild(script6);
      document.body.removeChild(script7);
     
    }
  }, []);

    const [formdata,setFormData]=useState({
       
        email:"",
        password:""
       
    });

    const [hidden,setHidden]=useState(true);

    
const { email, password}=formdata;
const onChange = e => setFormData({...formdata, [e.target.name]:e.target.value});

const onSubmit = async e => {
 e.preventDefault();
    
 
       
  onLoad();
  

    login({email,password});
    setTimeout(onLoads,2000);
    
}
const onLoad =()=>setHidden(false);
const onLoads = () =>setHidden(true);

if(isAuthenticated){
  return <Redirect to="/polls"></Redirect>

}


  return(
    
    <>
    
     

      {!hidden&&<div onMouseDown={()=>onLoads()}><Backdrop /><Spinner/></div>}

      
   
    <section className="container">
     

     
      
	<div className="limiter">
		<div className="container-login100">
			<div className="wrap-login100">
				<form className="login100-form"  onSubmit={e=>onSubmit(e)}>
					<span className="login100-form-title p-b-43">
						Login to continue
					</span>
					
					
					<div className="wrap-input100">
						<input className="input100" type="text" name="email" value={email} onChange={e=>onChange(e) }/>
						<span className="focus-input100"></span>
						<span className="label-input100">Email</span>
					</div>
					
					
					<div className="wrap-input100 " >
						<input className="input100" type="password" name="password" value={password} onChange={e=>onChange(e) }/>
						<span className="focus-input100"></span>
						<span className="label-input100">Password</span>
					</div>

					<div className="flex-sb-m w-full p-t-3 p-b-32">
						<div className="contact100-form-checkbox">
							<input className="input-checkbox100" id="ckb1" type="checkbox" name="remember-me"/>
							<label className="label-checkbox100" for="ckb1">
								Remember me
							</label>
						</div>

						<div>
            <Link to="/reset" className="txt1">
								Forgot Password?
							</Link>
						</div>
					</div>
			

					<div className="container-login100-form-btn">
						<button className="login100-form-btn">
							Login
						</button>
					</div>

          <div className="alertDiv pt-3 pb-0">
        <Alert/>
        </div> 
					
					<div className="text-center p-t-46 p-b-20">
						<span className="txt2">
							or sign up using
						</span>
					</div>

					<div className="login100-form-social flex-c-m">
						<a href="#" className="login100-form-social-item flex-c-m bg1 m-r-5">
							<i className="fa fa-facebook-f" aria-hidden="true"></i>
						</a>

						<a href="#" className="login100-form-social-item flex-c-m bg2 m-r-5">
							<i className="fa fa-twitter" aria-hidden="true"></i>
						</a>
					</div>
				</form>

				<div className="login100-more" style={{backgroundImage:url('images/bg-01.jpg')}}>
				  <div className="login100-more">
          </div>
        </div>
			</div>
		</div>
	</div>
     </section>
 </>
  )
  

}
const mapStateToProps = state =>{
  return{
    isAuthenticated:state.auth.isAuthenticated,
    loading:state.auth.loading
  }
}
export default connect(mapStateToProps,{login})(Login)
