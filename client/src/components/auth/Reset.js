import React, {useState} from 'react';

import {Link,Redirect} from 'react-router-dom';
import {connect} from 'react-redux';
import { setAlert } from '../../actions/alert';
import propTypes from 'prop-types';
import { reset } from '../../actions/auth';
import Alert from '../layout/Alert';

import Backdrop from '../backdrop/BackDropLoad';
import Spinner from '../layout/Spinner'
const Reset = ({reset,isAuthenticated}) =>{

    const [formdata,setFormData]=useState({
      
        email:"",
       
    });


    const [hidden,setHidden]=useState(true);
const {email}=formdata;
const onChange = e => setFormData({...formdata, [e.target.name]:e.target.value});

const onSubmit = async e => {
 e.preventDefault();
 
 
  onLoad();
   reset({email});
    
  setTimeout(onLoads,2000)
 
}
if(isAuthenticated){
  return <Redirect to='/polls'></Redirect>

}
if(localStorage.token){
  return <Redirect to="/polls"/>
   
}
const onLoad =()=>setHidden(false);
const onLoads = () =>setHidden(true);
  return(
    <>
    
    {!hidden&&<div onClick={()=>onLoads()}><Backdrop /><Spinner/></div>}

      
    <section className="container">
        <div className="alertDiv">
          <Alert/>
        </div>  
      <h1 className="large text-primary">Reset Password</h1>
      <p className="lead"><i className="fas fa-user"></i></p>
      <form className="form" onSubmit={e=>onSubmit(e)}>
       
        <div className="form-group">
          <input type="email" placeholder="Email Address" name="email" value={email} onChange={e=>onChange(e) }/>
          
        </div>
       
       
        <input type="submit" className="btn btn-primary" value="Submit" />
      </form>
     
    </section>
 </>
  );
  };
  Reset.propTypes={
    setAlert:propTypes.func.isRequired,
    register:propTypes.func.isRequired,
    isAuthenticated:propTypes.bool
  };
  const mapStateToProps = state =>{
    return{
      isAuthenticated:state.auth.isAuthenticated
    }
  }
export default connect(mapStateToProps,{ setAlert, reset })(Reset)
