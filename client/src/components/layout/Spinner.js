import React, { Fragment } from 'react';
import spinner from './spinner.gif';
import "./spinner.css";
export default () => (
  <Fragment>
    <img
      src={spinner}
      className='bl'
      alt='Loading...'
    />
  </Fragment>
);
