import React, {useEffect} from 'react';
import {connect} from 'react-redux';
import {Link,Redirect } from 'react-router-dom';
import {loadPollsItem,deleteCon} from '../../actions/poll';
import Spinner from '../layout/Spinner';
const PollItem = ({loadPollsItem,poll,deleteCon,pollslist,auth:{user,loading},match}) =>{

    useEffect(()=>{
        loadPollsItem(match.params.id)
    },[loadPollsItem,deleteCon,loading]);
    
   const DeleteCon=(id1,id2)=>{
    deleteCon(id1,id2);
    console.log("delet");
    loadPollsItem(match.params.id)
   return <Redirect to={`/item/polls/${match.params.id}`}/>

   } 
    
  return loading||pollslist==null ? (
    <Spinner />
  ) : (
    <div className="container-fluid">
      <div className="row">
          <div className="col-md-12">
              <div className="card">
                <span className="h1 text-center pt-2">
                  {poll.pollname}</span>
                 




                   <div className="card-body">
                     <Link to={`/addContestant/${match.params.id}`} className="btn btn-outline-primary mb-2" >Add Contestant</Link>
                      <table className="table table-bordered">
                        <tr className="bg-secondary text-light"><td>Contenstant Name</td><td>Votes</td><td>Action</td></tr>
                        {
                        poll.pollslist != null&&poll.pollslist.length>0&&
                        pollslist.map(item=>(

                        
                               <tr><td>{item.contenstantName}</td> <td> {item.votes.length}</td><td><button onClick={()=>DeleteCon(match.params.id,item._id)}  className="btn btn-danger">Delete</button></td></tr>
                          
                        ))
                        }
                        </table>
                    </div> 

               </div> 
          </div>  
      </div>  
      



    </div>
  )

}

const mapStateToProps = state =>{
  return {
    poll:state.poll.poll,
    pollslist:state.poll.poll.pollslist,
    auth:state.auth
  }
}


export default connect(mapStateToProps,{loadPollsItem,deleteCon})(PollItem)