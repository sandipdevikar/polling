import React,{useEffect} from 'react';
import {connect} from 'react-redux';
import {loadVideos} from '../../actions/poll'
const VideoHomePage = ({loadVideos,videos}) =>{

    useEffect(()=>{
        loadVideos();
    },[loadVideos]);
  return(
    <div className="row">

           
                {
                    videos&&videos.length>0&&
                    videos.map((video)=>(
                        <React.Fragment key={video._id}>
                        <div className="col-md-4 col-12 py-2">
                        

                        <video width="100%" height="240" controls>
                        <source src={`/${video.video}`} type="video/mp4"/>
                        Your browser does not support the video tag.
                        </video>
                        </div>
                                                </React.Fragment>
                    ))
                }
          
      
    </div>
  )

}

const mapStateToProps = state =>{
    return{
        videos:state.poll.video
    }
}

export default connect(mapStateToProps,{loadVideos})(VideoHomePage)