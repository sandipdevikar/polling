import React from 'react';
import {Link} from 'react-router-dom';
const GuestPollItem = ({loading , posts}) =>{

    if(loading){
        return <h2>loading...</h2>
    }else{
  return(
    <div>
            <ul className="list-group  mb-4">
                {
                    posts.map((poll)=>(
                 <div  key={poll._id}>  <Link  to={`/${poll._id}`}> <li className="list-group-item">{poll.pollname}</li></Link></div>
                    ))
                }

            </ul>
    </div>
  )

}
}
export default GuestPollItem