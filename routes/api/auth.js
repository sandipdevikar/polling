const express=require("express");
const router=express.Router();
const User = require("../../models/user");
const auth = require("../../middleware/auth");
const {check,validationResult}=require("express-validator/check");
const bcrypt = require("bcryptjs");
const config= require("config");
const jwt = require("jsonwebtoken");
const uploads = require("../../models/File");

 
const multer  = require('multer');
const GridFsStorage = require('multer-gridfs-storage');

const url=config.get("mongoURI");

// Create a storage object with a given configuration
const storage = new GridFsStorage({ url });
 
// Set multer storage engine to the newly created object
const upload = multer({ storage });
 

 
// Upload your files as usual
router.post('/profile', upload.single('file'), (req, res, next) => { 
    /*....*/ 
    console.log("hello");
    res.json({file:req.file});


})
 




//@Route GET api/auth
//Discription Authentication checking
//Access Public
router.get("/",auth,async (req,res)=>{

try {
   const user= await User.findById(req.user.id).select("-password");
    res.json(user)
} catch (err) {
    console.log(err.message);
    return res.status(500).json({msg:"server error"});

}    
})

//@Route POST api/auth
//Discription Login checking
//Access Public
router.post("/",[

    check("email","Invalid email").isEmail(),
    check("password","Invalid Password").exists()





], async (req,res)=>{
const errors=validationResult(req);
    if(!errors.isEmpty()){

        return res.status(400).json({msg:errors.array()});

    }
    try{
            const { email,password} =req.body;

            let user = await User.findOne({email});
            if(!user){

                return res.status(400).json({ errors: [{ msg: 'User not exists' }] });
            }
         const v=await bcrypt.compare(password,user.password);
         if(!v){
                return res.status(400).json({ errors: [{ msg: 'Invalid password' }] });
         }

         const payload = {
            user: {
              id: user.id
            }
          };
    
          jwt.sign(
            payload,
            config.get('jwtSecret'),
            { expiresIn: 360000 },
            (err, token) => {
              if (err) throw err;
              res.json({ token });
            }
          );
    }catch(err){

    }
});

router.post('/file',async (req,res)=>{
  
  const {uploadfile} = req.body;
try{

  let uploadfiles = new uploads({
    uploadfile
  })
  await uploadfiles.save();
  res.json(uploadfiles)
}catch(err){
console.log(err.message);
return res.status(500).json({msg:"server error"});
}

});
router.get('/file',async (req,res)=>{
  try {
    const polls= await Poll.find();
     res.json(polls)
 } catch (err) {
     console.log(err.message);
     return res.status(500).json({msg:"server error"});
 
 }    
});

module.exports=router;
