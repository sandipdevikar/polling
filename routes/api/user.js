const express=require("express");
const gravatar = require("gravatar");
const bcrypt = require("bcryptjs");
const jwt=require("jsonwebtoken");
const {check,validationResult} = require("express-validator/check");
const router=express.Router();
const User = require("../../models/user")
const config=require("config");
const crypto = require('crypto');
const {sendWelcomeMail,sendPasswordResetMail} = require('../../mail/mailer')
router.post("/",[
    check("name","Name is require").not().isEmpty(),
    check("email","Email is invalid").isEmail(),
    check("password","password min 6 length").isLength({min:6})




], async (req,res)=>{
        const errors=validationResult(req);
        if(!errors.isEmpty()){
           return res.status(400).json({msg : errors.array()});
        }
        const {
            name,
            email,
            password
        }=req.body;
        
        try {
            let user= await User.findOne({email});
            if (user) {
                return res
                  .status(400)
                  .json({ errors: [{ msg: 'User already exists' }] });
              }
             const avatar = gravatar.url(email,{
                s: '200',
                r: 'pg',
                d: 'mm'
             });

             user = new User({
                 name,email,avatar,password
             });
             const salt = await bcrypt.genSalt(10);

              user.password= await bcrypt.hash(password,salt);
            
              await user.save();
              const payload = {
                user: {
                  id: user.id
                }
              };
        
              jwt.sign(
                payload,
                config.get('jwtSecret'),
                { expiresIn: 360000 },
                (err, token) => {
                  if (err) throw err;
                  res.json({ token });
                 return sendWelcomeMail(email,name);
                

                }
              );
        } catch (err) {
            console.error(err);
          return  res.status(500).send('Server error');  
        }
    
});
router.post("/reset",[
    
    check("email","Email is invalid").isEmail()



], async (req,res)=>{
        const errors=validationResult(req);
        if(!errors.isEmpty()){
           return res.status(400).json({msg : errors.array()});
        }
        const {
           
            email
          
        }=req.body;
        
        try {
            let user= await User.findOne({email});
            if (!user) {
                return res
                  .status(400)
                  .json({ errors: [{ msg: 'User not exists' }] });
              }
            

             
             
              const payload = {
                user: {
                  id: user.id
                }
              };
        
              jwt.sign(
                payload,
                config.get('jwtSecret'),
                { expiresIn: 360000 },
                async (err, token) => {
                  if (err) throw err;
                  user.resetToken = token;
                  user.resetTokenExpiration = Date.now() + 3600000;
                
                  await user.save();
                  
                 res.json({msg:"check your mail link has sent"})
                return sendPasswordResetMail(token,email);

                }
              );
        } catch (err) {
            console.error(err);
          return  res.status(500).send('Server error');  
        }
    
});

router.post('/reset/password',async (req,res)=>{

  const {token, password} = req.body;
   
  jwt.verify(token, config.get('jwtSecret'), function(err, decoded) {

    if(err){

     return res.status(400).json({errors:[{msg:"not found"}]})

    }
   
  });

  try {

   

  let user = await User.findOne({resetToken:token, resetTokenExpiration: { $gt: Date.now() }});
 
  
  if(!user){
   
   return res.status(400).json({errors:[{msg:"user not found"}]})

  }

  const salt = await bcrypt.genSalt(10);
  

  user.password= await bcrypt.hash(password,salt);
 
await user.save();

return res.json({msg:"successfully Reset password, Log In please"})

    
  } catch (error) {
    console.log(error);
    return  res.status(500).send('Server error');  
     
  }


})

module.exports=router;